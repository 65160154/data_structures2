package LinkedList;

public class FristLastList {
    private Link first; // ref to first link
    private Link last; // ref to last link

    public void FirstLastList() // constructor
    {
        first = null; // no links on the list yet
        last = null;
    }

    public boolean isEmpty() // true if no links
    {
        return first == null;
    }

    public void insertFirst(long dd) // insert at the front of the list
    {
        Link newLink = new Link(dd); // make a new link
        if (isEmpty()) // if an empty list,
            last = newLink; // newLink <-- last
        newLink.next = first; // newLink --> old first
        first = newLink; // first --> newLink
    }

    public void insertLast(long dd) // insert at the end of the list
    {
        Link newLink = new Link(dd); // make a new link
        if (isEmpty()) // if an empty list,
            first = newLink; // first --> newLink
        else
            last.next = newLink; // old last --> newLink
        last = newLink; // newLink <-- last
    }

    public long deleteFirst() // delete the first link
    { // (assumes a non-empty list)
        long temp = first.dData;
        if (first.next == null) // if only one item
            last = null; // null <-- last
        first = first.next; // first --> old next
        return temp;
    }

    public void displayList() {
        System.out.print("List (first-->last): ");
        Link current = first; // start at the beginning
        while (current != null) // until the end of the list,
        {
            current.displayLink(); // print data
            current = current.next; // move to the next link
        }
        System.out.println("");
    }

}
